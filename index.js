// 3. GET ALL 
	fetch('https://jsonplaceholder.typicode.com/todos')
		.then((response) => response.json())
		.then((json) => console.log(json))


//4. FETCH JUST THE TITLE. 
	fetch('https://jsonplaceholder.typicode.com/todos')
		.then((res) => res.json())
		.then((json) => console.log(json.map( toDoList => toDoList.title)))
//5. FETCH SINGLE ITEM 
	fetch('https://jsonplaceholder.typicode.com/todos/1')
		.then((response) => response.json())
		.then((json) => console.log(json))

/*
6. Using the data retrieved, print a message in the console that will provide 
   the title and status of the to do list item. 
*/
	fetch('https://jsonplaceholder.typicode.com/todos')
		.then((res) => res.json())
		.then((json) =>
			json.forEach((toDoList) => {
				console.log(`${toDoList.title} ${toDoList.completed}`)
			})
		)
/*
7. Using POST method create a to do list item.
*/
	fetch('https://jsonplaceholder.typicode.com/todos',{
		method: 'POST',
		headers: {
			'Content-Type': 'application/JSON'
		},
		body: JSON.stringify({
			id: 300,
			title: "Develop a Website", 
			userId: 300,
			completed: true
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

/*
8. Using PUT method that will update 
*/
	fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method: 'PUT',
		headers: {
			'Content-Type': 'application/JSON'
		},
		body: JSON.stringify({
			completed: true
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

// 9. update a todo list item by changing the data structure.
	fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method: 'PUT',
		headers: {
			'Content-Type': 'application/JSON'
		},
		body: JSON.stringify({
			userId: 1,
			title: "this.updated",
			description: "this.description",
			completed: true,
			dateCompleted: new Date()
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

// 10.PATCH METHOD.
	fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/JSON'
		},
		body: JSON.stringify({
			title: "this.title"
		})
	})
	.then((res) => res.json())
	.then((json) => console.log(json))

// 11. ADD DATE WHEN STATUS IS COMPLETE 
	fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method: 'PUT',
		headers: {
			'Content-Type': 'application/JSON'
		},
		body: JSON.stringify({
			completed: true,
			dateCompleted: new Date()
		})
	})
	.then((res) => res.json())
	.then((json) => console.log(json))

// 12-15 exported file in a1.
